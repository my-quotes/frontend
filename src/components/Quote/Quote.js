import React from "react"

import styles from "./Quote.module.scss"

export default function Quote(props) {
	const { quote, by } = props
	return (
		<article className={styles.quote}>
			<p className={styles["quote-text"]}>{quote}</p>
			<span className={styles["quote-by"]}>{by}</span>
		</article>
	)
}
