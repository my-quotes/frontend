import React from "react"

import styles from "./Navbar.module.scss"
import Button from "../Button/Button"

export default function Navbar() {
	return (
		<div className={styles.navbar}>
			<h1 className={styles["brand-name"]}>Brilliant Quotes</h1>
			<div className={styles.buttons}>
				<Button text="Create" type="red" to="/create" />
				<Button text="Read" type="blue" to="/" />
				<Button text="Update" type="green" to="/update" />
				<Button text="Delete" type="yellow" to="/delete" />
				<Button text="Random" type="secondary" to="/random" />
			</div>
		</div>
	)
}
