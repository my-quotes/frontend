import React from "react"
import { Link } from "react-router-dom"

import styles from "./Button.module.scss"

export default function Button(props) {
	const { text, to, type, size } = props
	if (to !== undefined)
		return (
			<Link
				to={to}
				className={`${styles.btn} ${type ? styles[`btn-${type}`] : ""}`}
			>
				{text}
			</Link>
		)
	return (
		<div className={`${styles.btn} ${type ? styles[`btn-${type}`] : ""}`}>
			{text}
		</div>
	)
}
