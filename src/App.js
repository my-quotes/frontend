import React, { Component } from "react"
import { Route, Switch } from "react-router-dom"

import styles from "./App.module.scss"
import Navbar from "./components/Navbar/Navbar"
import Create from "./scenes/Create/Create"
import Read from "./scenes/Read/Read"
import Update from "./scenes/Update/Update"
import Delete from "./scenes/Delete/Delete"
import Random from "./scenes/Random/Random"
import Footer from "./components/Footer/Footer"

class App extends Component {
	render() {
		return (
			<div className={styles.App}>
				<Navbar />
				<div className={styles.container}>
					<Switch>
						<Route path="/create" component={Create} />
						<Route exact path="/" component={Read} />
						<Route path="/update" component={Update} />
						<Route path="/delete" component={Delete} />
						<Route path="/random" component={Random} />
					</Switch>
				</div>
				<Footer />
			</div>
		)
	}
}

export default App
