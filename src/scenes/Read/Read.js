import React from "react"

import styles from "./Read.module.scss"
import Quote from "../../components/Quote/Quote"

export default function Read() {
	return (
		<div className={styles.read}>
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
			<Quote quote="Test" by="Momchil Kolev" />
		</div>
	)
}
